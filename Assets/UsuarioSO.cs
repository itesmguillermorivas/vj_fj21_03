﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UsuarioSO : MonoBehaviour
{
    public ScriptableExample infoDeEnemigo;
    public AudioClip[] soniditos;

    private AudioSource player;

    // Start is called before the first frame update
    void Start()
    {
        print(infoDeEnemigo.nombre + " " + infoDeEnemigo.vida);
        infoDeEnemigo.Saludar();

        player = GetComponent<AudioSource>();

        GameObject.DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.A))
            CambiarAudio(0);

        if (Input.GetKeyUp(KeyCode.S))
            CambiarAudio(1);

        if (Input.GetKeyUp(KeyCode.D))
            CambiarAudio(2);

        if (Input.GetKeyUp(KeyCode.F))
            CambiarAudio(3);

        if (Input.GetKeyUp(KeyCode.G))
            CambiarAudio(4);

        if (Input.GetKeyUp(KeyCode.Z))
            player.Pause();

        if (Input.GetKeyUp(KeyCode.X))
            player.Stop();

        if (Input.GetKeyUp(KeyCode.Q))
            Application.Quit();

        if (Input.GetKeyUp(KeyCode.W)) {
            // SceneManager.LoadScene(1);
            SceneManager.LoadScene("Dummy");
        }
            

    }

    private void CambiarAudio(int indice) {

        player.clip = soniditos[indice];
        //player.loop = true;
        player.Play();

    }
}
