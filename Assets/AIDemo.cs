﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AIDemo : MonoBehaviour
{
    public float threshold;
    public GameObject enemigo;

    // states
    State pelear, dormido, pasear, truco;

    // symbols
    Symbol llamar, alimentar, acariciar;

    // initial state
    State current;
    MonoBehaviour currentBehaviour;



    // Start is called before the first frame update
    void Start()
    {
        // initialize states
        pelear = new State("Pelear", typeof(PelearBehaviour));
        dormido = new State("Dormido", typeof(DormidoBehaviour));
        pasear = new State("Pasear", typeof(PasearBehaviour));
        truco = new State("Truco", typeof(TrucoBehaviour));

        // initialize symbols
        llamar = new Symbol("Llamar");
        alimentar = new Symbol("Alimentar");
        acariciar = new Symbol("Acariciar");

        // set transitions
        pelear.AddTransition(acariciar, truco);
        pelear.AddTransition(llamar, dormido);

        dormido.AddTransition(alimentar, pelear);
        dormido.AddTransition(llamar, truco);

        pasear.AddTransition(acariciar, pasear);
        pasear.AddTransition(llamar, dormido);

        truco.AddTransition(acariciar, dormido);
        truco.AddTransition(alimentar, pelear);

        // set initial state
        //current = pelear;
        ChangeState(pelear);

        // add behaviour
        // gameObject.AddComponent<PelearBehaviour>();
        // gameObject.AddComponent(typeof(PelearBehaviour));
        StartCoroutine(ChecarDistancia());

        // PRUEBAS CON SINGLETON
        // GameObject.Find(""); // mandan string de nombre, regresa game object
        // GameObject.FindObjectOfType<>() // obtienen un GO con algún componente específico incluido

        EjemploSingleton ejemplo = EjemploSingleton.Instance;
        print(ejemplo.velocidad);

        Transform ejemploTransform = EjemploSingleton.Instance.transform;
        // Transform ejemploTransform2 = EjemploSingleton.Instance.gameObject.GetComponent;
    }

    void ChangeState(Symbol symbol)
    {
        // apply symbol
        State newState = current.ApplySymbol(symbol);

        ChangeState(newState);

    }

    void ChangeState(State newState) {

        // if equals, do nothing
        if (current == newState)
            return;

        // if current was already set remove component
        if (current != null && currentBehaviour != null)
            Destroy(currentBehaviour);

        // in any case update values
        current = newState;
        currentBehaviour = gameObject.AddComponent(current.Behaviour) as MonoBehaviour;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.L))
            ChangeState(llamar);
        */

        if (Input.GetKeyDown(KeyCode.A))
            ChangeState(alimentar);

        if (Input.GetKeyDown(KeyCode.C))
            ChangeState(acariciar);

        //print(current.Name);

    }

    IEnumerator ChecarDistancia() {

        while (true) {

            float distancia = Vector3.Distance(transform.position, enemigo.transform.position);

            if(distancia < threshold)
                ChangeState(llamar);

            yield return new WaitForSeconds(0.5f);
        }
    }
}
