﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "instancita", menuName = "misObjetitos/scriptableUno")]
public class ScriptableExample : ScriptableObject
{

    public float vida;
    public string nombre;
    public float defensa;
    public float ataque;

    public GameObject gameObject;
    public Transform transform;

    public void Saludar() {
        Debug.Log("HOLA!");
    }
}
