﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploSingleton : MonoBehaviour
{
    // singleton - limitar instancias de este objeto a solo una

    // instancia no-estática
    // método estático para accederla
    public static EjemploSingleton Instance {
        get;
        private set;
    }

    public float velocidad = 5;
    public float dummy1 = 2;
    public float dummy2 = 1;

    private void Awake()
    {
        // normalmente el control de instancias es preventivo (evitamos nuevas instancias)

        // aqui es correctivo (osea, borramos nuevas instancias)

        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

    }


    // Start is called before the first frame update
    void Start() {


        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
